﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:cmd="http://www.clarin.eu/cmd/"
    xmlns:cmd1="http://www.clarin.eu/cmd/1"
    exclude-result-prefixes="cmd1">
    
    <xsl:param name="core"/>
    
    <!-- identity copy -->
    
    <xsl:template match="@*" mode="#all" priority="0">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="*" mode="#all" priority="0">
        <xsl:element name="cmd:{local-name()}">
            <xsl:apply-templates select="@*|node()"/>
        </xsl:element>
    </xsl:template>
    
    <!-- handle schema location -->
    
    <xsl:template match="/*:CMD/@xsi:schemaLocation">
        <xsl:attribute name="xsi:schemaLocation">
            <xsl:text>http://www.clarin.eu/cmd/ http://catalog.clarin.eu/ds/ComponentRegistry/rest/registry/profiles/</xsl:text>
            <xsl:value-of select="$core/CMD_ComponentSpec/Header/ID"/>
            <xsl:text>/xsd</xsl:text>
        </xsl:attribute>
    </xsl:template>

    <!-- handle version -->

    <xsl:template match="/*:CMD/@CMDVersion" priority="1">
        <xsl:attribute name="CMDVersion" select="'1.1'"/>
    </xsl:template>
    
    <!-- handle ref -->
    <xsl:template match="@cmd1:ref" priority="1">
        <xsl:attribute name="ref" select="."/>
    </xsl:template>

    <xsl:template match="/*:CMD">
        <cmd:CMD>
            <xsl:apply-templates select="@*"/>
            <xsl:comment>
                <xsl:text>[stripToCore] @xsi:schemaLocation="</xsl:text>
                <xsl:value-of select="@xsi:schemaLocation"/>
                <xsl:text>"</xsl:text> 
            </xsl:comment>
            <xsl:if test="@CMDVersion!='1.1'">
                <xsl:comment>
                <xsl:text>[stripToCore] @CMDVersion="</xsl:text>
                <xsl:value-of select="@CMDVersion"/>
                <xsl:text>"</xsl:text> 
            </xsl:comment>
            </xsl:if>
            <xsl:apply-templates select="node()"/>
        </cmd:CMD>
    </xsl:template>
        
    <!-- start traversing the components -->
    
    <xsl:template match="/*:CMD/*:Components/*">
        <!-- replace the instance profile node by the core profile node -->
        <xsl:comment>[stripToCore] added core profile node</xsl:comment>
        <xsl:element namespace="http://www.clarin.eu/cmd/" name="{$core/CMD_ComponentSpec/CMD_Component/@name}">
            <xsl:comment>
                <xsl:text>[stripToCore] </xsl:text> 
                <xsl:text>&lt;</xsl:text>
                <xsl:value-of select="name()"/>
                <xsl:text>&gt;</xsl:text>
            </xsl:comment>
            <xsl:apply-templates mode="core">
                <xsl:with-param name="core" select="$core/CMD_ComponentSpec/CMD_Component/*" tunnel="yes"/>
            </xsl:apply-templates>
            <xsl:comment>
                <xsl:text>[stripToCore] </xsl:text> 
                <xsl:text>&lt;/</xsl:text>
                <xsl:value-of select="name()"/>
                <xsl:text>&gt;</xsl:text>
            </xsl:comment>
        </xsl:element>        
    </xsl:template>
    
    <!-- match a node -->
    
    <xsl:template match="*" mode="core" priority="1">
        <xsl:param name="core" tunnel="yes"/>
        <xsl:variable name="cur" select="local-name()"/>
        <xsl:choose>
            <xsl:when test="$core/@name = $cur">
                <xsl:element name="cmd:{$cur}">
                    <xsl:variable name="attr-names" select="$core/AttributeList/Attribute/Name"/>
                    <xsl:apply-templates select="@ref | @cmd1:* | @*[name()=$attr-names]"/>
                    <xsl:for-each select="@* except (@ref | @cmd1:* | @*[name()=$attr-names])">
                        <xsl:comment>
                            <xsl:text>[stripToCore] @</xsl:text>
                            <xsl:value-of select="name()"/>
                            <xsl:text>="</xsl:text>
                            <xsl:value-of select="."/>
                            <xsl:text>"</xsl:text> 
                        </xsl:comment>
                    </xsl:for-each>
                    <xsl:apply-templates select="node()" mode="core">
                        <xsl:with-param name="core" select="$core[@name = $cur]/(CMD_Component|CMD_Element)" tunnel="yes"/>
                    </xsl:apply-templates>
                </xsl:element>
            </xsl:when>
            <xsl:otherwise>
                <xsl:comment>
                    <xsl:text>[stripToCore] </xsl:text>
                    <xsl:apply-templates select="." mode="copy"/>
                </xsl:comment>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <!-- make a commented copy of a node -->
    
    <xsl:template match="*" mode="copy" priority="1">
        <xsl:text>&lt;</xsl:text>
        <xsl:value-of select="name()"/>
        <xsl:apply-templates select="@*" mode="copy"/>
        <xsl:text>&gt;</xsl:text>
        <xsl:apply-templates select="node()" mode="copy"/>
        <xsl:text>&lt;/</xsl:text>
        <xsl:value-of select="name()"/>
        <xsl:text>&gt;</xsl:text>
    </xsl:template>
        
    <xsl:template match="@*" mode="copy" priority="1">
        <xsl:text> </xsl:text>
        <xsl:value-of select="name()"/>
        <xsl:text>="</xsl:text>
        <xsl:value-of select="."/>
        <xsl:text>"</xsl:text>
    </xsl:template>
        
</xsl:stylesheet>